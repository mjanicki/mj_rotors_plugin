#!/usr/bin/env python

from __future__ import division
from __future__ import print_function, unicode_literals
import rospy
import pygame
from geometry_msgs.msg import TransformStamped, Transform, Twist
from trajectory_msgs.msg import MultiDOFJointTrajectory,\
    MultiDOFJointTrajectoryPoint
import numpy as np
from pyquaternion import Quaternion
from threading import RLock
from copy import deepcopy


class Controller(object):
    def __init__(self, mav_name, frequency, linear_velocity_reference,
                 angular_velocity_reference):
        self._frequency = frequency
        self._linear_velocity_reference = linear_velocity_reference
        self._angular_velocity_reference = angular_velocity_reference
        self._yaw_velocity = 0.
        self._local_linear_velocity = np.zeros(3)

        self._current_position = Position()
        self._current_position_lock = RLock()
        self._position_subscriber = self._get_position_subscriber(mav_name)
        self._target_position = Position()
        self._trajectory_publisher = self._get_trajectory_publisher(mav_name)

    def _get_trajectory_publisher(self, mav_name):
        return rospy.Publisher('/' + mav_name + '/command/trajectory',
                               MultiDOFJointTrajectory, queue_size=1)

    def _get_position_subscriber(self, mav_name):
        return rospy.Subscriber('/' + mav_name + '/ground_truth/transform',
                                TransformStamped, self._position_callback)

    def _position_callback(self, msg):
        t = msg.transform.translation
        q = msg.transform.rotation

        with self._current_position_lock:
            self._current_position.location = np.array([t.x, t.y, t.z])
            self._current_position.attitude = Quaternion(q.w, q.x, q.y, q.z)

    def spin(self):
        r = rospy.Rate(self._frequency)
        while not rospy.is_shutdown():
            controller._get_input()
            controller._update()
            r.sleep()

    def _get_input(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_w:
                    self._local_linear_velocity[0] += \
                        self._linear_velocity_reference
                elif event.key == pygame.K_s:
                    self._local_linear_velocity[0] -= \
                        self._linear_velocity_reference
                elif event.key == pygame.K_a:
                    self._yaw_velocity += angular_speed
                elif event.key == pygame.K_d:
                    self._yaw_velocity -= angular_speed
                elif event.key == pygame.K_UP:
                    self._local_linear_velocity[2] += \
                        self._linear_velocity_reference
                elif event.key == pygame.K_DOWN:
                    self._local_linear_velocity[2] -= \
                        self._linear_velocity_reference
                elif event.key == pygame.K_LEFT:
                    self._local_linear_velocity[1] += \
                        self._linear_velocity_reference
                elif event.key == pygame.K_RIGHT:
                    self._local_linear_velocity[1] -= \
                        self._linear_velocity_reference
                elif event.key == pygame.K_ESCAPE:
                    quit()

            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_w:
                    self._local_linear_velocity[0] -= \
                        self._linear_velocity_reference
                elif event.key == pygame.K_s:
                    self._local_linear_velocity[0] += \
                        self._linear_velocity_reference
                elif event.key == pygame.K_a:
                    self._yaw_velocity -= self._angular_velocity_reference
                elif event.key == pygame.K_d:
                    self._yaw_velocity += self._angular_velocity_reference
                elif event.key == pygame.K_UP:
                    self._local_linear_velocity[2] -= \
                        self._linear_velocity_reference
                elif event.key == pygame.K_DOWN:
                    self._local_linear_velocity[2] += \
                        self._linear_velocity_reference
                elif event.key == pygame.K_LEFT:
                    self._local_linear_velocity[1] -= \
                        self._linear_velocity_reference
                elif event.key == pygame.K_RIGHT:
                    self._local_linear_velocity[1] += \
                        self._linear_velocity_reference

    def _update(self):
        with self._current_position_lock:
            current_position = deepcopy(self._current_position)
        self._target_position.attitude = current_position.attitude
        velocity = self._get_target_linear_velocity()
        self._target_position.location += velocity / self._frequency

        self._publish_target_trajectory()

    def _get_target_linear_velocity(self):
        z_rotation = deepcopy(self._target_position.attitude)
        z_rotation[1] = 0
        z_rotation[2] = 0
        z_rotation = z_rotation.normalised
        target_linear_velocity = z_rotation.rotate(self._local_linear_velocity)
        return target_linear_velocity

    def _publish_target_trajectory(self):
        target_trajectory = Trajectory()

        transform = target_trajectory.points[0].transforms[0]
        transform.rotation.w = self._target_position.attitude[0]
        transform.rotation.x = self._target_position.attitude[1]
        transform.rotation.y = self._target_position.attitude[2]
        transform.rotation.z = self._target_position.attitude[3]
        transform.translation.x = self._target_position.location[0]
        transform.translation.y = self._target_position.location[1]
        transform.translation.z = self._target_position.location[2]

        angular_velocity = target_trajectory.points[0].velocities[0].angular
        angular_velocity.z = self._yaw_velocity * np.pi / 180

        self._trajectory_publisher.publish(target_trajectory)


class Trajectory(MultiDOFJointTrajectory):
    def __init__(self):
        MultiDOFJointTrajectory.__init__(self)
        self.joint_names = ['base_link']
        self.points = [MultiDOFJointTrajectoryPoint()]
        self.points[0].transforms = [Transform()]
        self.points[0].velocities = [Twist()]
        self.points[0].accelerations = [Twist()]


class Position(object):
    def __init__(self):
        self.location = np.zeros(3)
        self.attitude = Quaternion()


rospy.init_node('rotors_controller', anonymous=True)
linear_speed = rospy.get_param('~linear_speed', .5)
angular_speed = rospy.get_param('~angular_speed', 50)
mav_name = rospy.get_param('~mav_name', 'firefly')
rate = rospy.get_param('~rate', 10)

pygame.display.set_mode((100, 100))

controller = Controller(mav_name, rate, linear_speed, angular_speed)
controller.spin()
