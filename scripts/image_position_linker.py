#!/usr/bin/env python
from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals
import rospy
from geometry_msgs.msg import TransformStamped
from sensor_msgs.msg import Image
from mj_rotors_plugin.msg import ImageWithOriginPosition
from threading import RLock
from time import sleep
from copy import deepcopy


class Linker(object):
    def __init__(self):
        self._publisher = rospy.Publisher('~output', ImageWithOriginPosition,
                                          queue_size=1)

        self._transform_lock = RLock()
        self._transform_msg = None
        self._transform_subscriber = rospy.Subscriber('~transform_input',
                                                      TransformStamped,
                                                      self._position_callback)
        self._wait_for_first_transform()
        self._image_subscriber = rospy.Subscriber('~image_input', Image,
                                                  self._image_callback)

    def _wait_for_first_transform(self):
        while self._transform_msg is None:
            sleep(0)

    def _position_callback(self, msg):
        with self._transform_lock:
            self._transform_msg = msg

    def _image_callback(self, image):
        image_with_origin = self._get_image_with_origin(image)
        disparity = self._get_disparity_seconds(image_with_origin)
        if abs(disparity) > .1:
            msg = '{}:\nimage is {:.2f} seconds seconds late. '\
                  'Skipping image.'.format(rospy.get_name(), disparity)
            rospy.logerr(msg)
            return

        self._publisher.publish(image_with_origin)

    def _get_image_with_origin(self, image):
        with self._transform_lock:
            image_with_origin = ImageWithOriginPosition()
            image_with_origin.transform_stamped = deepcopy(self._transform_msg)
        image_with_origin.image = image
        return image_with_origin

    def _get_disparity_seconds(self, image_with_origin):
        img_time = image_with_origin.image.header.stamp
        transform_time = image_with_origin.transform_stamped.header.stamp
        secs = transform_time.secs - img_time.secs
        nsecs = transform_time.nsecs - img_time.nsecs
        return secs + nsecs / 10**9


rospy.init_node('image_position_linker', anonymous=True)
Linker()
rospy.spin()
